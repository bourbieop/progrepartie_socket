GOODWIN Jean-Paul
AHMED Sahim

Chat IRC

2.0.1
TCP : mode connecté
UDP : non connecté


2.1.1
1.Arguments pour l'appel à socket : famille, numéro de port, adresse ip


2.
structure de sockaddr_in :

struct sockaddr_in {
   uint8_t         sin_len;       /* longueur totale      */
   sa_family_t     sin_family;    /* famille : AF_INET     */
   in_port_t       sin_port;      /* le numéro de port    */
   struct in_addr  sin_addr;      /* l'adresse internet   */
   unsigned char   sin_zero[8];   /* un champ de 8 zéros  */
};


3. endianess : c'est quoi?

Prenons par exemple 6 en base 10
-> cela donne 110 en base 2 .
Mais attention, certains ordinateurs ne liront pas 110 de la même façon ce qui donnera 96 en base 10.

0 0 0 0 0 1 1 0
-> Big endian
<- little endian

Pour les processeurs x64 --> Big-endian
Pour les processeurs x86 --> little-endian

remarque : tous les protocoles TCP/IP communiquent en big-endian

Pour la compilation du fichier : gcc -Wall -lpthread source.c -o res