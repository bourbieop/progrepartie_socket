#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <netdb.h>
#include "sys/queue.h"

#define MSG_SIZE sizeof(msg_t)
#define BCAST_ADDR "192.168.3.255" //ou #define BCAST_ADDR(192<<24 + 168<<16 + 3<<8 + 255)
//htons(BCAST_ADDR)
#define BCAST_PORT 5000
#define BUF_SIZE 1024 /* 1024 char */
#define NICK_DEFAULT "Guest"
#define NICK_LEN 256
#define NODE_INFO_LEN 256

/****************************DEFINITION STRUCTURES****************************/

struct sockaddr bcast_addr;

typedef enum msg_type
{
    MT_INVAL = 0,
    MT_HELLO = 1,
    MT_MSG   = 2,
    MT_NICK  = 3,
    MT_COLOR = 4,
    MT_MAX,
} msg_type_t;

typedef struct msg
{
    msg_type_t  type;
    uint16_t    len;
    char        buf[BUF_SIZE];
} msg_t;

typedef struct user
{
    TAILQ_ENTRY(user)       lh; /* list handle */
    struct sockaddr_in      sa;
    char                    nick[NICK_LEN];
    char                    node_info[NODE_INFO_LEN];
    const char             *color;
} user_t;

/****************************VARIABLES****************************/

int bcast_sock = -1;
int user_online = 0;

/****************************PROTOTYPE FONCTION****************************/

int init_bcast(struct sockaddr *bcast_addr);

/****************************FONCTIONS****************************/

int init_bcast(struct sockaddr *bcast_addr)
{
    int sockfd = socket(AF_INET,SOCK_DGRAM, 0);
    

    int sock = -1;
    sock = bind(sockfd, bcast_addr, sizeof(struct sockaddr));
    
    int broadcastEnable=1;
	setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(int));
    
    if (sock==-1)
    {
    	printf("Erreur\n");
    }

	printf("ca marche\n");
    return sockfd;
}

msg_t *get_buf(msg_type_t type)
{
    msg_t *msg = NULL;

    char* buffer = malloc((BUF_SIZE+1) * sizeof(char));
    int byteRead;
    int i = 0;
    
    while ((byteRead = recv(bcast_sock, buffer, BUF_SIZE, 0)) > 0)
    {
    	if (byteRead <= 0)
    		break;
    	else
    	{
	    	msg->buf[i] = *buffer;
	}
		i++;
    }
    msg->buf[i+1] = '\0';
    msg->type = type;
    msg->len = i;
    
    return msg;
}


user_t *add_user(struct sockaddr_in *sa)
{
    user_t *user = NULL;
	//allocation memoire pour 1 user
    user = malloc(sizeof (user_t));
	if (user !=NULL)
	{
		user->sa = *sa;
		strncpy(user->nick , NICK_DEFAULT, NICK_LEN);
		get_node_info(user, sa);
		users[user_online] = *user;
		user_online++;
	}

    DBG("Registering new peer %s", user->node_info);

    TAILQ_INSERT_TAIL(&users_list, user, lh);
    return user;
}

void free_buf(msg_t *buf)
{
	free(buf);
}

void get_node_info(user_t *user, struct sockaddr_in *si)
{
    
	//a voir , utiliser getnameinfo pour initialiser adresse ip et port
	int port = sa->sin_port ;
	int *ip = inet_ntoa(sa->sin_addr);
	sprintf(user->node_info, "%d", port);
	strcat(user->node_info, ":");
	strcat(user->node_info, ip);
	char *name_inf = getname(info
}

/****************************MAIN****************************/

int main()
{
	bcast_sock = init_bcast(&bcast_addr);
	return 0;
}
