#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <netdb.h>

#include "sys/queue.h"

#ifdef DEBUG
#define DBG(fmt, args...)                                               \
    do {                                                                \
        fprintf(stderr, "(%d)[%14s:%d] "fmt"\n", getpid(), __func__, __LINE__, ## args); \
    } while (0)
#else
#define DBG(...) (void)0
#endif

#define FAIL_IF(cond, fmt, args...)                                     \
    do {                                                                \
        if (cond) {                                                     \
            DBG(fmt, ## args);                                          \
            exit(EXIT_FAILURE);                                         \
        }                                                               \
    } while (0)


#define BCAST_ADDR "192.168.10.255"
#define BCAST_PORT 1234

typedef enum msg_type
{
    MT_INVAL = 0,
    MT_HELLO = 1,
    MT_MSG   = 2,
    MT_NICK  = 3,
    MT_COLOR = 4,
    MT_MAX,
} msg_type_t;

#define NICK_DEFAULT "Guest"
#define NICK_LEN 256
#define NODE_INFO_LEN 256
typedef struct user
{
    TAILQ_ENTRY(user)       lh; /* list handle */
    struct sockaddr_in      sa;
    char                    nick[NICK_LEN];
    char                    node_info[NODE_INFO_LEN];
    const char             *color;
} user_t;

#define BUF_SIZE 1024 /* 1024 char */
typedef struct msg
{
    msg_type_t  type;
    uint16_t    len;
    char        buf[BUF_SIZE];
} msg_t;
#define MSG_SIZE sizeof(msg_t)

#define CMD_NICK "/n "
#define CMD_COLOR "/c "
#define CMD_SHOW "/s"

#define COLOR_NAME_LEN 256
typedef struct color_desc
{
    char name[COLOR_NAME_LEN];
    char code[256];
} color_desc_t;


/*------------------------------------------------------
 *  Global variables
 *-----------------------------------------------------*/

const struct color_desc colors[] =
{
    {"default", "\e[0m"}, /* Default color */
    {"black", "\e[0;30m"},
    {"red", "\e[0;31m"},
    {"green", "\e[0;32m"},
    {"yellow", "\e[0;33m"},
    {"blue", "\e[0;34m"},
    {"purple", "\e[0;35m"},
    {"cyan", "\e[0;36m"},
};
int colors_size = sizeof(colors) / sizeof(colors[0]);
#define DEFAULT_COLOR colors[0].code

int bcast_sock = -1;
struct sockaddr bcast_addr;

bool shutdown_req = false;  /* shutdown requested */
TAILQ_HEAD(mylist, user) users_list;

/*------------------------------------------------------
 *  Functions
 *-----------------------------------------------------*/

void get_node_info(user_t *user, struct sockaddr_in *si)
{
    /* À compléter */
}

user_t *add_user(struct sockaddr_in *sa)
{
    user_t *user = NULL;

    /* À compléter */

    DBG("Registering new peer %s", user->node_info);

    TAILQ_INSERT_TAIL(&users_list, user, lh);
    return user;
}

void del_user(user_t *user)
{
    TAILQ_REMOVE(&users_list, user, lh);
}

user_t *lookup_user(struct sockaddr_in *sa)
{
    user_t *user = NULL;

    TAILQ_FOREACH(user, &users_list, lh)
    {
        /* À compléter */
    }
    return NULL;
}

void show_users()
{
    user_t *user = NULL;

    TAILQ_FOREACH(user, &users_list, lh)
    {
        /* À compléter */
    }
}

msg_t *get_buf(msg_type_t type)
{
    msg_t *msg = NULL;

    /* À compléter */

    return msg;
}

void free_buf(msg_t *buf)
{
    /* À compléter */
}

int init_bcast(struct sockaddr *bcast_addr)
{
	/* A completer */
    int sock = -1;

    return sock;
}

void *hello_thread(void *args)
{
    while (!shutdown_req)
    {
        /* À compléter */

        DBG("hello message sent!");
        sleep(5);
    }

    return NULL;
}


void process_received_msg(user_t *user, msg_t *msg)
{
    switch(msg->type)
    {
        case MT_HELLO:
            {
                /* À compléter */
            }
            break;

        case MT_NICK:
            {
                /* À compléter */
            }
            break;

        case MT_MSG:
            {
                /* À compléter */
            }
            break;

        case MT_COLOR:
            {
                /* À compléter */
            }
            break;

        default:
            FAIL_IF(1, "Invalid message type");
            break;
    }
}

void *receive_thread(void *arg)
{
    while (!shutdown_req)
    {
        struct sockaddr_in sender_addr;
        user_t *user;
        msg_t *msg = NULL;

        /* À compléter */

        user = lookup_user(&sender_addr);
        if (!user)
        {
            /* À compléter */
        }

        /* À décommenter */
        //process_received_msg(user, msg);

    }
    return NULL;
}

void send_msg(char *data, size_t data_len)
{
    /* À compléter */
}

void enter_loop(void)
{
    while (1)
    {
        char str[BUF_SIZE];

        fgets(str, BUF_SIZE, stdin);

        send_msg(str, BUF_SIZE);
    }
}

int main(int arc, char **argv)
{
    pthread_t receive_th;   /* tid for receive thread */
    pthread_t hello_th;   /* tid for hello thread */
    int rc;

    TAILQ_INIT(&users_list);

    bcast_sock = init_bcast(&bcast_addr);

    rc = pthread_create(&hello_th, NULL, hello_thread, NULL);
    FAIL_IF(rc < 0, "Cannot create hello thread");

    rc = pthread_create(&receive_th, NULL, receive_thread, NULL);
    FAIL_IF(rc < 0, "Cannot create receive thread");

    enter_loop();

    /* Requires shutdown */
    shutdown_req = true;
    pthread_join(receive_th, NULL);
    pthread_join(hello_th, NULL);

    return EXIT_SUCCESS;
}
